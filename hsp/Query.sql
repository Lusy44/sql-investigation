/*
1.1 Выбрать в таблице Orders заказы, которые были доставлены после 4 мая 1998 года (колонка ShippedDate) включительно 
и которые доставлены с ShipVia >= 2.  Запрос должен высвечивать только колонки OrderID, ShippedDate и ShipVia.
*/
SELECT
  OrderID,
  ShippedDate,
  ShipVia
FROM
  Orders
WHERE
  ShippedDate > '1998-05-04' AND ShipVia >= 2;
  
/*
1.2 Написать запрос, который выводит только недоставленные заказы из таблицы Orders. В результатах запроса высвечивать
для колонки ShippedDate  вместо значений NULL строку ‘Not Shipped’ – использовать системную функцию CASЕ. Запрос должен
высвечивать только колонки OrderID и ShippedDate.
*/
SELECT
  OrderID,
  (
    CASE WHEN ShippedDate IS NULL THEN "Not Shipped"
  END
) AS ShippedDate
FROM
  Orders
WHERE
  ShippedDate IS NULL;
  

/*
1.3 Выбрать в таблице Orders заказы, которые были доставлены после 1 мая 1998 года (ShippedDate) не включая эту дату
или которые еще не доставлены. В запросе должны высвечиваться только колонки OrderID (переименовать в OrderNumber) и 
ShippedDate. В результатах запроса высвечивать для колонки ShippedDate вместо значений NULL строку ‘Not Shipped’, 
для остальных значений высвечивать дату в следующем формате "День недели Месяц Год" (использовать функцию DATE_FORMAT)
*/
SELECT
  OrderID AS OrderNumber,
  (
    CASE WHEN ShippedDate IS NULL THEN "Not Shipped" WHEN ShippedDate > '1998-05-01' THEN(
      DATE_FORMAT(ShippedDate,
      "%a" "-" "%c" "-" "%Y")
    )
  END
) AS ShippedDate
FROM
  Orders
WHERE
  ShippedDate > '1998-05-01' OR ShippedDate IS NULL;
  
  
/*
2.1 Выбрать из таблицы Customers всех заказчиков, проживающих в USA и Canada. Запрос сделать с только помощью оператора IN. Высвечивать 
колонки с именем пользователя и названием страны в результатах запроса. Упорядочить результаты запроса по имени заказчиков и по месту проживания
*/
SELECT
  CustomerID,
  Country
FROM
  Customers
WHERE
  Country IN('USA',
  'CANADA')
ORDER BY
  ContactName, 
  Country
 
/*
2.2 Выбрать из таблицы Customers всех заказчиков, не проживающих в USA и Canada. Запрос сделать с помощью оператора IN. 
 Высвечивать колонки с именем пользователя и названием страны в результатах запроса. Упорядочить результаты запроса по имени заказчиков.
*/ 
 SELECT
  CustomerID,
  Country
FROM
  Customers
WHERE
  Country NOT IN('USA',
  'CANADA')
ORDER BY
  ContactName
 

/*
2.3 Выбрать из таблицы Customers все страны, в которых проживают заказчики. Страна должна быть упомянута только один раз и список
 отсортирован по убыванию. Не использовать предложение GROUP BY. Высвечивать только одну колонку в результатах запроса. 
*/
SELECT DISTINCT
  Country
FROM
  Customers
ORDER BY
  Country DESC
 
 
/*
3.1 Выбрать все заказы (OrderID) из таблицы Order Details (заказы не должны повторяться), где встречаются продукты с количеством от 3 до 10
 включительно – это колонка Quantity в таблице Order Details. Использовать оператор BETWEEN. Запрос должен высвечивать только колонку OrderID.
*/
 SELECT DISTINCT
  OrderID
FROM
  Order_Details
WHERE
  Quantity BETWEEN 3 AND 10;


/*
3.2 Выбрать всех заказчиков из таблицы Customers, у которых название страны начинается на буквы из диапазона b и g. Использовать оператор BETWEEN. 
Проверить, что в результаты запроса попадает Germany. Запрос должен высвечивать только колонки CustomerID и Country и отсортирован по Country.
*/
SELECT
  CustomerID,
  Country
FROM
  Customers
WHERE
  Country BETWEEN "b" AND "h"
ORDER BY
  Country


/*
3.3 Выбрать всех заказчиков из таблицы Customers, у которых название страны начинается на буквы из диапазона b и g, не используя оператор BETWEEN. 
Запрос должен высвечивать только колонки CustomerID и Country и отсортирован по Country. 
*/
SELECT
  CustomerID,
  Country
FROM
  Customers
WHERE
  Country REGEXP '^[B-G]'
ORDER BY
  Country
  
 
/*  
4.1 В таблице Products найти все продукты (колонка ProductName), где встречается подстрока 'chocolade'. Известно, что в подстроке 'chocolade' может быть
изменена одна буква 'c' в середине - найти все продукты, которые удовлетворяют этому условию. Подсказка: результаты запроса должны высвечивать 2 строки. 
*/
 SELECT
  ProductName
FROM
  Products
WHERE
  ProductName LIKE '%cho_olade%'
  
  
/* 
5.1 Найти общую сумму всех заказов из таблицы Order Details с учетом количества закупленных товаров и скидок по ним. Результат округлить до сотых и высветить
в стиле 1 для типа данных money. Скидка (колонка Discount) составляет процент из стоимости для данного товара. Для определения действительной цены на проданный
продукт надо вычесть скидку из указанной в колонке UnitPrice цены. Результатом запроса должна быть одна запись с одной колонкой с названием колонки 'Totals'.
*/ 
 SELECT
  ROUND(
    SUM(
      (1 - Discount) * UnitPrice * Quantity
    ),
    2
  ) AS Totals
FROM
  Order_Details
  
  
/*
5.2 По таблице Orders найти количество заказов, которые еще не были доставлены (т.е. в колонке ShippedDate нет значения даты доставки).
Использовать при этом запросе только оператор COUNT. Не использовать предложения WHERE и GROUP.
*/
SELECT
  COUNT(OrderID) - COUNT(ShippedDate) AS Count_Order_Without_Delivered
FROM
  Orders;
  
  
/*  
5.3 По таблице Orders найти количество различных покупателей (CustomerID), сделавших заказы. Использовать функцию COUNT и не использовать 
предложения WHERE и GROUP.
*/
  SELECT
  COUNT(DISTINCT CustomerID) AS CustomerID
FROM
  Orders
  
  
/*  
6.1 По таблице Orders найти количество заказов с группировкой по годам. В результатах запроса надо высвечивать две колонки c названиями Year и Total. 
Написать проверочный запрос, который вычисляет количество всех заказов.
*/
 SELECT
  YEAR(OrderDate) AS YEAR,
  COUNT(OrderID) AS Total
FROM
  Orders
GROUP BY
  YEAR(OrderDate);
  
 SELECT
  COUNT(OrderID) AS Total_AMOUNT
FROM
  Orders
  
  
/*  
6.2 По таблице Orders найти количество заказов, cделанных каждым продавцом. Заказ для указанного продавца – это любая запись в таблице Orders, где в колонке
EmployeeID задано значение для данного продавца. В результатах запроса надо высвечивать колонку с именем продавца (Должно высвечиваться имя полученное 
конкатенацией LastName & FirstName. Эта строка LastName & FirstName должна быть получена отдельным запросом в колонке основного запроса. Также основной запрос
должен использовать группировку по EmployeeID.) с названием колонки ‘Seller’ и колонку c количеством заказов высвечивать с названием 'Amount'. Результаты запроса
должны быть упорядочены по убыванию количества заказов.
*/ 
SELECT
  (
  SELECT
    CONCAT_WS("-",
    LastName,
    FirstName)
  FROM
    Employees
  WHERE
    Employees.EmployeeID = Orders.EmployeeID
) AS Seller,
COUNT(OrderID) AS Amount
FROM
  Orders
GROUP BY
  Seller
ORDER BY
  Amount DESC 
  
  
/*  
6.3 Найти покупателей и продавцов, которые живут в одном городе. Если в городе живут только один или несколько продавцов или только один или несколько покупателей,
то информация о таких покупателя и продавцах не должна попадать в результирующий набор. Не использовать конструкцию JOIN. В результатах запроса необходимо вывести 
следующие заголовки для результатов запроса: ‘Person’, ‘Type’ (здесь надо выводить строку ‘Customer’ или ‘Seller’ в завимости от типа записи), ‘City’. Отсортировать 
результаты запроса по колонке ‘City’ и по ‘Person’. (Вы должны использовать UNION)
*/
SELECT
  CONCAT_WS("-",
  lastName,
  FirstName) AS Person,
  City,
  'Seller' AS TYPE
FROM
  Employees
WHERE
  City = ANY(
SELECT
  City
FROM
  Customers
)
UNION
SELECT
  ContactName,
  City,
  'Customer'
FROM
  Customers
WHERE
  City = ANY(
SELECT
  City
FROM
  Employees
)
ORDER BY
  City,
  Person
  
  
/*  
6.4 Найти всех покупателей, которые живут в одном городе. В запросе использовать соединение таблицы Customers c собой - самосоединение. Высветить колонки CustomerID и City.
Запрос не должен высвечивать дублируемые записи. Для проверки написать запрос, который высвечивает города, которые встречаются более одного раза в таблице Customers. 
Это позволит проверить правильность запроса.
*/
SELECT DISTINCT
 A.CustomerID,
 A.City
FROM
  Customers A,
  Customers B
WHERE
  A.CustomerID <> B.CustomerID AND A.City = B.City
ORDER BY
  A.City;
  
 SELECT City FROM Customers
GROUP BY City
HAVING COUNT(city) > 1;
  
  
/*
 * 6.5 По таблице Employees найти для каждого продавца его руководителя, т.е. кому он делает репорты. Высветить колонки с именами 'UserName' (LastName) и 'Boss'. 
 * В колонках должны быть высвечены имена из колонки LastName.
 */
SELECT
  A.LastName AS UserName,
  B.LastName AS Boss
FROM
  Employees A,
  Employees B
WHERE
  B.EmployeeID = A.ReportsTo;
  
  
/*
 * 7.1 Определить продавцов, которые обслуживают регион 'Western' (таблица Region). Результаты запроса должны высвечивать два поля: 'LastName' продавца и название 
 * обслуживаемой территории ('TerritoryDescription' из таблицы Territories). Запрос должен использовать JOIN в предложении FROM. Для определения связей между таблицами 
 * Employees и Territories надо использовать графические диаграммы для базы Northwind.  
 */
 SELECT
  TerritoryDescription,
  Employees.LastName
FROM
  (
    (
      (
        Territories
      INNER JOIN
        Region ON Territories.RegionID = Region.RegionID
      )
    INNER JOIN
      EmployeeTerritories ON Territories.TerritoryID = EmployeeTerritories.TerritoryID
    )
  INNER JOIN
    Employees ON EmployeeTerritories.EmployeeID = Employees.EmployeeID
  )
WHERE
  Region.RegionDescription = 'Western'
  
  
/*
 *8.1 Высветить в результатах запроса имена всех заказчиков из таблицы Customers и суммарное количество их заказов из таблицы Orders. Принять во внимание, 
 *что у некоторых заказчиков нет заказов, но они также должны быть выведены в результатах запроса. Упорядочить результаты запроса по возрастанию количества заказов.  
 */
SELECT
  ContactName AS CustomerName,
  COUNT(Orders.OrderID) AS Amount
FROM
  Customers
LEFT OUTER JOIN
  Orders ON Customers.CustomerID = Orders.CustomerID
GROUP BY
  ContactName
ORDER BY
  Amount
  
  
/*
 * 9.1 Высветить всех поставщиков колонка CompanyName в таблице Suppliers, у которых нет хотя бы одного продукта на складе (UnitsInStock в таблице Products равно 0).
 *  Использовать вложенный SELECT для этого запроса с использованием оператора IN.
 */
SELECT
  CompanyName
FROM
  Suppliers
WHERE
  SupplierID IN(
  SELECT
    SupplierID
  FROM
    Products
  WHERE
    UnitsInStock = 0
);


/*
 * 10.1    Высветить всех продавцов, которые имеют более 150 заказов. Использовать вложенный коррелированный SELECT.
 */
SELECT
  (
  SELECT
    CONCAT_WS("-",
    LastName,
    FirstName)
  FROM
    Employees
  WHERE
    EmployeeID = Orders.EmployeeID
) AS Seller,
COUNT(OrderID) AS Amount
FROM
  Orders
GROUP BY
  Seller
HAVING
  Amount > 150
  
  
/*
 * 11.1 Высветить всех заказчиков (таблица Customers), которые не имеют ни одного заказа (подзапрос по таблице Orders).
 * Использовать коррелированный SELECT и оператор EXISTS.
 */
 SELECT
  ContactName
FROM
  Customers
WHERE NOT EXISTS
  (
  SELECT
    CustomerID
  FROM
    Orders
  WHERE
    CustomerID = Customers.CustomerID
)

/*
 * 12.1 Для формирования алфавитного указателя Employees высветить из таблицы Employees список только тех букв алфавита,
 * с которых начинаются фамилии Employees (колонка LastName ) из этой таблицы. Алфавитный список должен быть отсортирован по возрастанию.
 */
 SELECT DISTINCT
  LEFT(LastName,
  1) AS First_Letter
FROM
  Employees
ORDER BY
  First_Letter
 
 
 
 
 